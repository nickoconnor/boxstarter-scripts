Write-Output "- Setting computer name to workstation"
Rename-Computer -computername (hostname) -newname workstation

Write-Output "- You must now reload this VM using a vagrant-reload plugin command in your Vagrantfile"

Exit 0
